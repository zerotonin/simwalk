# easy run test script
import SW_simulation, SW_movementMath,time,cProfile
import numpy as np
#make simulation object
#swSim   = SW_simulation.walkSim(animalNo=2,animalNames=['Gauss','Levy'])
swSim   = SW_simulation.walkSim(animalNo=1,animalNames='Levy')
#make start positions
startX = np.array([-100.,100.])
startA = np.array([0.,0.])
startP = np.column_stack((startX,startX))
startP = np.column_stack((startP,startA))
# start
t =time.time()
#swSim.startSimulation(startPos=startP,moveMode=['gaussian','cauchy'] ,maxTime=10)
cProfile.run(swSim.startSimulation(moveMode='cauchy' ,maxTime=10))
t =time.time() -t
#print swSim.animals[0].foodTotal,swSim.animals[1].foodTotal
print t,swSim.animals[0].foodTotal
        