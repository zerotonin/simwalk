# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 18:18:38 2017

@author: bgeurten
"""

from math import cos, sin
import numpy as np

class movementMath:
    def __init__(self, parent, stepModel = 'flat', stepVelRange = (-20,20)):
        """
        This is a collection of useful 2D trigonometry functions that are later used 
        to compute new positions etc.
        
        """
        self.parent         = parent
        self.stepModel      = stepModel
        self.stepVelRange   = stepVelRange
    
    def rotateVector(self,thrust,angle):
        """
        Calculate a Ficksche rotation matrix to turn a vector
        
        / x' \   /  cos theta  sin theta \ / x \
        |    | = |                       | |   |
        \ y' /   \ - sin theta cos theta / \ y /
        
        @origin
        """
        
        cos_theta, sin_theta = cos(angle), sin(angle)
        x = thrust
        y = 0

        return (x * cos_theta - y * sin_theta,
                x * sin_theta + y * cos_theta)

    def generateThrustFlat(self):
        '''
        Generate a step velocity with flat random distribution between 
        stepVelRange[0] and stepVelRange[1].
        '''
        return (np.random.rand()*self.stepVelRange[1]*2) + self.stepVelRange[0]
       
    
    def generateThrustGaussian(self, mu = 0, sigma = 0.3):
        '''
        Generate a step velocity with a gaussia distribution between stepVelRange[0]
        and stepVelRange[1]. The gaussians default values are mu = 0 and sigma 0.3.
        '''
        thrust = np.random.normal(mu, sigma, 1) 
        thrust = thrust[0]*self.stepVelRange[1]
        return self.clipAtVelExt(thrust)

    def generateThrustCauchy(self):
        '''
        Generate a step with the numpy standard cauchy distribution
        between stepVelRange[0] and stepVelRange[1]
        '''
        thrust = np.random.standard_cauchy(1)
        return self.clipAtVelExt(thrust[0])

                
    def clipAtVelExt(self,thrust):
        '''

        '''
        if thrust > self.stepVelRange[1]:
            return self.stepVelRange[1]
        elif thrust < self.stepVelRange[0]:
            return self.stepVelRange[0]
        else:
            return thrust

    def generateStep(self):
        """
        Generate a Step        
        """
        #choose step model and generate thrust accordingly
        if   self.stepModel == 'flat':
            thrust = self.generateThrustFlat()
        elif self.stepModel == 'gaussian':
            thrust = self.generateThrustGaussian()
        elif self.stepModel == 'cauchy':
            thrust = self.generateThrustCauchy()
        else:
            print 'In SW_movementMath.generateStep() a wrong step model was choosen: ' + self.stepModel

        # This only describes a half circle, but as the velocity boundries are symmetrical around
        # zero all directions are included.  0-180 deg * +- thrust

        angle = np.random.rand()*np.pi

        # rotate thrust vector
        step  = self.rotateVector(thrust,angle)

        return (step,angle)

    def durFunc(self,x_p):    
        """
        This function calculates the fit of the fit duration in dependence of the 
        temperature and rearing temperature of the ADULT animal.  The function 
        consitits of two gaussian fits for the temperature domain and a 2nd
        degree polynom for the p Values. In this case both the temperature domain 
        and the p-values are dependent on the rearing temperature.
        
        @param x_p float random value between  0 and 1
        @param x_t float Drosophila body temperature
        @param d   list of floats containing the rearing temperature depending 
                parameters
        @return list of floats with new step duration values
        """
        x_t  = 25.0
        v,d  = self.calcParameters(18.0)
        res  = (self.gauss(x_t,d[0],34.0,d[1])+self.gauss(x_t,d[2],18,d[3])) * self.poly3(x_p,d[4],d[5],d[6],d[7]) 
        return res
        
    def poly3(self,x,a,b,c,d):    
        """
        Implementation of a 3rd degree polynom
            
                3      2
        f(x) = a x  + b x  + c x + d
        
        @param x     float x - value(s as list)   
        @param a     float see above 
        @param b     float see above 
        @param c     float see above 
        @param d     float see above 
        @return list of floats with the results corresponding to the x-values
        """
        return a*x**3+b*x**2+c*x+d

    def poly2(self,x,b,c,d):    
        """
        Implementation of a 3rd degree polynom
            
                   2
        f(x) =  b x  + c x + d
        
        @param x     float x - value(s as list)   
        @param a     float see above 
        @param b     float see above 
        @param c     float see above 
        @param d     float see above 
        @return list of floats with the results corresponding to the x-values
        """
        return b*x**2+c*x+d

    def linear(self,x,c,d):    
        """
        Implementation of a 3rd degree polynom
            
        f(x) =   c x + d
        
        @param x     float x - value(s as list)   
        @param a     float see above 
        @param b     float see above 
        @param c     float see above 
        @param d     float see above 
        @return list of floats with the results corresponding to the x-values
        """
        return c*x+d

    def gauss(self,x,a,x0,sigma):    
        """
        Implementation of a Gaussian distribution
            
                            2
                    (x - x0)
                - ------------
                            2
                    (2.0*sigma)
        f(x) = a*e 
        
        @param x     float x - value(s as list)   
        @param a     float factor before the gaussian 
        @param x0    float mu of the distribution
        @param sigma float sigma of the distribution
        @return list of floats with the results corresponding to the x-values
        
        """
        return a*np.exp(-np.power((x-x0)/(2.*sigma), 2.))
    
    def calcParameters(self,T):
        """
        This function returns the rearing temperature depending parameters to 
        adjust the velocity and step duration functions. Most parameters can be fit
        with a 2nd degree polynom. As only 3 different rearing temperatures were 
        used the fits might not be to reliable and it is saver to stay with the 
        three measured rearing temperatures: 18,25,30 °C
        
        @param T float rearing temperature
        @return lists two lists 1) v velocity parameters 2) d duration parameters
        """
        if (T == 'larval'):
            v = []
            d = []
        else:
        
            v =np.empty(3)
            v[0]    = self.poly2(T, 0.12099083,     -5.66690323,      70.7646241)
            v[1]    = self.poly2(T,-3.74142886e-02,  2.11954454e+00,  -4.34662354e+01)
            v[2]    = self.poly2(T,-0.07461945,      3.14576967,     -22.92563255)
            
            
            d =np.empty(8)
            d[0]  = self.linear(T, 0.12916312,  -2.812052 )
            d[1]  = self.poly2( T, 0.14534352,  -7.95321438, 110.60810592)
            d[2]  = self.poly2( T, 0.02662904,  -1.28284224,  15.65186953)
            d[3]  = self.poly2( T,11.66468748,-641.51376808,8753.61384674)
            d[4]  = self.poly2( T, 0.14445866,  -6.92317723,  75.43250874)
            d[5]  = self.poly2( T,-0.24815044,  11.88971369,-129.60720025)
            d[6]  = self.poly2( T ,0.15978689,  -7.64871786,  83.63873179)
            d[7]  = self.poly2( T,-0.0612502,    2.92865988, -32.21078329)
            
        return v,d

