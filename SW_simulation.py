# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 18:15:22 2017

@author: bgeurten
"""


import numpy as np
import math
from scipy import optimize 
import numpy.matlib as ml
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import SW_animal


class walkSim:
    def __init__(self,worldSize=(400,400),temporalFreq = 500,animalNo = 1, animalNames = list()):
        """
        This is the central simulation class it runs the steps of the differnt 
        walking simulations. It also assigns food and trajectories to the 
        animals.
        
        @size   : This is the size of the world in mm as a tupel coordinates are
                  half that size in pos and negative direction so for example a world
                  of (2000,6000) has coordinates for x from -1000 to +1000 and for y 
                  from -3000 to +3000 mm. Our agent has the typical Drosophila body
                  length of 2 mm
        @animals: A list of animal objects
        @food   : A two dimensional list of the coordinates of food items
        @foraged: 
        """
        self.size         = worldSize
        self.animals      = list()
        self.animalNames  = animalNames
        self.food         = list()
        self.foodMax      = np.Infinity # eat as much as you want
        self.temporalFreq = temporalFreq # Hertz
        self.timeStep     = 1000/self.temporalFreq #ms
        self.time         = 0 #ms
        self.timeMax      = 10000 # simulate a 10 seconds


        # create list of simulated animals
        for i in range (animalNo):
            if len(self.animalNames) == 0:
                self.animals.append(SW_animal.walkSimAnimal(self,i))
            else:
                self.animals.append(SW_animal.walkSimAnimal(self,self.animalNames[i]))
        
    def newInterationStep(self):
        """
        This is the main function. The iteration step duration is marked as the
        smallest mean duration of one of the PMs. The general ID is that the 
        system runs as follows:
        This means we use the se
        
        STEP 1: Plan new step
        This means we use the self.animals[0].move() function which generates a 
        movement in a fashion determined by  self.animals(0).moveMode.
        
        STEP 2: Execute new step
        Assign Path based on the new movement and the current position of the 
        animal we calculate its new trajectory and ending point with
        self.assignTrajectory
        
        STEP 3: Forage on the path
        Calculate the hull of the animal along each trajectory point and see 
        which food positions are inside the polygon. Those are assigned to the 
        animal and deleted from the simulation environment
        
        STEP 4: Check the clock 
        Add step duration to simulation and check if the time is up, if there 
        is still time left on the clock. Check if there is still food in the 
        simulation, if not end the simulation as well.     
        
        @param 
        """
        goOnSimulate = np.ones((len(self.animals),1))

        #check out all animals
        for i in range(len(self.animals)):
            #shorthand
            animal = self.animals[i]

            # first check if the food or time criteria is reached
            if animal.time >= self.timeMax:
                goOnSimulate[i] = 0
            if animal.foodTotal >= self.foodMax:
                goOnSimulate[i] = 0

            # if the animal still needs to move do so
            if goOnSimulate[i] == 1:
                
                #add animal time
                animal.time += self.timeStep
                #check if the animal can still use planned movements
                if animal.futureTraj.shape[0] > 1:
                    step = animal.futureTraj[0,:]
                    animal.futureTraj = animal.futureTraj[1::,:]
                    animal.position = step
                    animal.trajectory = np.row_stack((animal.trajectory,step))
                    animal.getForage()
                elif animal.futureTraj.shape[0] > 0:
                    step = animal.futureTraj[0,:]
                    animal.futureTraj = np.ones(0)
                    animal.position = step
                    animal.trajectory = np.row_stack((animal.trajectory,step))
                    animal.getForage()
                else:
                    animal.makeFutureTraj()
        return goOnSimulate
    
    def startSimulation(self,startPos = np.array([0.,0.,0.]),moveMode = 'flat',foodMode=(100,'linearAuto'),maxFood = 'max',maxTime=np.Infinity):
        
        #setting food
        self.setFood(foodMode[0],foodMode[1])
        
        # set up animal movement types
        self.setMotionType4Animals(moveMode)

        # set up animal start positions
        self.setStartPositions(startPos)

        # check and set end-criteria
        startStim = True
        if maxFood == 'max':
            maxFood = len(self.food)
        if maxFood == np.Infinity and maxTime == np.Infinity:
            print "Both end criteria are set to infinity the simulation would run forever, so it will not be started!"
            startStim = False
        
        #startStim
        if startStim == True:
            self.foodMax = maxFood
            self.timeMax = maxTime*1000 #from sec to ms
            goOnSimulate = np.ones((len(self.animals),1))
            while np.sum(goOnSimulate) != 0:
                goOnSimulate = self.newInterationStep()
                
            

        

            
        
    def setFood(self,amount,distributionMode):
        """
        This is a initialisation routine that places food in the simulation. 
        There are three different distribution modes: linear, or randomised and clustered
        you can assign how much food is placed there.
        @param 
        @param 
        """
        if distributionMode == 'linearAuto':
            self.setFoodLinearAuto()
    
    def setFoodLinearAuto(self):
        '''
        This function places linearly distributed food. 
        '''
        # We do not go to the one border completely because we do not want a special
        # emphasis during hyper space jumps. Otherwise if you cross a border you get 
        # twice the food as you get the one on the neg. and on the pos. border.
        xList = np.linspace(self.size[0]/-2.0,self.size[0]/2.0-1,self.size[0])
        yList = np.linspace(self.size[1]/-2.0,self.size[1]/2.0-1,self.size[1])
        
        # repeat and tile to get all possible versions
        yList = np.repeat(yList,self.size[0]+1)
        xList = np.tile(xList,self.size[1]+1)
        
        # make a 2D mat out of it and return it to self.food
        self.food = np.column_stack((xList,yList))
                    
    def setStartPositions(self,positions):
        
        for i in range(len(self.animals)):
            if len(positions.shape) > 1:
                self.animals[i].position = positions[i,:]

            else:
                self.animals[i].position = positions
                    
    
    def setMotionType4Animals(self,motionTypes):
        
        for i in range(len(self.animals)):
            if type(motionTypes) == list:
                self.animals[i].moveMath.stepModel = motionTypes[i]
            else:
                self.animals[i].moveMath.stepModel = motionTypes
                    


    


        

