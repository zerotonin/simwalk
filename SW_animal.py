# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 18:18:38 2017

@author: bgeurten
"""

import numpy as np
import math
from scipy import optimize 
import numpy.matlib as ml
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import SW_movementMath
import time

class walkSimAnimal:
    def __init__(self,parent,id_name):
        """
        This is an animal object it usually has all the animal specific data 
        and moves in the designed patterns. It also keeps all the foraged food.
        
        
        @body       :
        @PM         :
        @HMM        :
        @storage    :
        @position   :
        @trajectory :
        @trajInterp :
        @moveMode   :
        
        """
        self.body        = (1.,2.)
        self.PM          = []
        self.PMnext      = []
        self.PMcurrent   = []
        self.HMM         = []
        self.storage     = []
        self.position    = np.array([0.,0.,0.])        
        self.trajectory  = np.array([0.,0.,0.])
        self.futureTraj  = np.ones(0)
        self.trajInterp  = []
        self.name        = id_name
        self.parent      = parent
        self.moveMath    = SW_movementMath.movementMath(self)
        self.time        = 0 # ms
        self.foodTotal   = 0 # items

    def makeFutureTraj(self):
        
        #check if this is a prototypical movement
        if self.moveMath.stepModel == 'HMM':
            pass
        else:
            # generate a step that means get the speed vector and the angle wich 
            # the animal has to turn before taking of in that direction
            newStep    = self.moveMath.generateStep()
            speedVec   = np.array(newStep[0])
            angle      = newStep[1]

            # get the step duration
            newStepDur = self.moveMath.durFunc(np.random.rand()) 
            #speed times time gives you the position the animal will end up in
            newTargetVec = speedVec*newStepDur
            # turning in a random walk fashion means that the whole turning is 
            # done before the step takes place. So here we turn with 1 deg 
            # resolution to forage all the food needed.
            turnStepNo = np.round(np.abs(self.position[2]-angle)/np.deg2rad(1))
        
            t = time.time()
            if turnStepNo > 0:
                turningAng = np.linspace(self.position[2],angle,turnStepNo)
                for stepAng in turningAng:
                    self.position[2] = stepAng
                    self.getForage()
                    

            #make future trajectory; has to be multiplied by 1000 to become ms
            # divided
            stepNo = np.round((newStepDur*1000)/ self.parent.timeStep)
            if stepNo < 1:
                stepNo == 1

            # interpolate the positions in between
            xpos   = np.linspace(self.position[0],newTargetVec[0],stepNo)
            ypos   = np.linspace(self.position[1],newTargetVec[1],stepNo)
            angle  = np.linspace(self.position[2],angle,stepNo)
           
            #save to future trajectory
            self.futureTraj = np.column_stack((xpos,ypos))

            #check positional data for hyper space jumps
            for i in xrange(self.futureTraj.shape[1]):
                self.futureTraj[i,:] = self.jump2hyperSpaceTest(self.futureTraj[i,:])

            #save angle
            self.futureTraj = np.column_stack((self.futureTraj,angle))




    
    def consume(self):
        """
        Consume food if to take another step. Optional to implement
        @param 
        @param 
        """
        pass
        
    def move(self):
        """
        General movement function that executes one of the other movement 
        functions based on the value of self.moveMode.
        @param 
        @param 
        """
        pass
    
    def assignBodyShape(self,longAxis=2,points=50):
        """
        Function that assigns an eliptic body to the animal of different size
        @longAxis: long Axis of the animal in mm
        @points  : number of coordinates on the hull
        """
        pass

    def computeNewCoordinate(self,thrust,slip,yaw,duration,oldCoords):

        """
        This function calculates the new coordinate 
        @thrust   : forward velocity in mm/s (float) 
        @slip     : sideways velocity in mm/s (float)
        @yaw      : yaw velocity in rad/s (float)
        @duration : duration of the movement in seconds (float)
        @oldCoords: old coordinates in mm as a tupel of floats

        return value new coordinates (tupel of floats)
        """

        pass
    
    
    def getForage(self):
        
        """the food items foraged from the food item list
        self.food
        """
        ell_center = np.array([self.position[0],self.position[1]])
        angleRAD   = self.position[2]
        width      = self.body[0]
        height     = self.body[1]
        # calc Angle trig
        cos_angle = np.cos(angleRAD)
        sin_angle = np.sin(angleRAD)

        # transfer food into elipse centric coordinatensys
        xc = self.parent.food[:,0] - ell_center[0]
        yc = self.parent.food[:,0] - ell_center[1]

        # rotate ellipse centric coordinate system so that aligns with long axis
        xct = xc * cos_angle - yc * sin_angle
        yct = xc * sin_angle + yc * cos_angle 

        # now normalise coordinates of food so that height is one on the height axis
        # and width is one on the width axis.xrange
        rad_cc = (xct**2/(width/2.)**2) + (yct**2/(height/2.)**2)

        foraged = np.where(rad_cc < 1)

        # save foraged food
        self.foraged = self.parent.food[foraged,:]
        # delete foraged food from main list
        self.parent.food    = np.delete(self.parent.food,foraged,axis=0)
        #increase food total
        self.foodTotal += len(foraged)

    def jump2hyperSpaceTest(self,target):
        """
        If you fall of one boundary I set you up on the opposing one.
        
        @param target a tupel of floats which discribes the new target
               position
        @param 
        """
        # return variable pre allocation
        newTarget = np.array([0,0])
        
        # go through dimensions and check against border size
        for i in range(2):
            # check if it went over the positive edge
            if target[i] > self.parent.size[i]:
                offSet = target[i] - self.parent.size[i]
                newX = -1.0*self.parent.size[i] + offSet
            # check if it went over the negative edge
            elif target[i] < -1.0*self.parent.size[i]:
                offSet = target[i] + self.parent.size[i]
                newX = self.parent.size[i] + offSet
            # be happy:
            else:
                newX = target[i]
            # save tested coordinate
            newTarget[i] = newX
        # return checked coordinates
        return newTarget    